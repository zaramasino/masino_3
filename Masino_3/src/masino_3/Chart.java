/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package masino_3;

//imports
import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

/**
 * This program creates a chart object and contains methods to retrieve some
 * integral information (largely inspired by class example)
 *
 * @author @zaramasino
 *
 */
public class Chart extends JFrame {
    //Initializes variables finalChart, plot, and renderer
    private final JFreeChart finalChart;
    private final XYPlot plot;
    private final XYLineAndShapeRenderer renderer;
    private TimeSeriesCollection dataset;
    private TimeSeries OTTemp;
    private TimeSeries CATemp ;
    private TimeSeries MOTemp;
    private TimeSeries MOPrec;
    private TimeSeries OTPrec;
    private TimeSeries CAPrec;

    /**
     *constructor for the chart that sets the series shapes to squares as default
     * @throws IOException
     */
    public Chart() throws IOException {
        finalChart = createChartPanel();
        plot = (XYPlot) finalChart.getPlot();
        renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesShape(0, new Rectangle2D.Double(-3.0, -3.0, 6.0, 6.0));
        renderer.setSeriesShape(1, new Rectangle2D.Double(-3.0, -3.0, 6.0, 6.0));
        renderer.setSeriesShape(2, new Rectangle2D.Double(-3.0, -3.0, 6.0, 6.0));
        renderer.setSeriesShape(3, new Rectangle2D.Double(-3.0, -3.0, 6.0, 6.0));
        renderer.setSeriesShape(4, new Rectangle2D.Double(-3.0, -3.0, 6.0, 6.0));
        renderer.setSeriesShape(5, new Rectangle2D.Double(-3.0, -3.0, 6.0, 6.0));

        plot.setRenderer(renderer);
    }

    /**
     *a method that creates a jfreechart with the specified files
     * credit: https://www.jfree.org/forum/viewtopic.php?t=23480
     * credit:  https://stackabuse.com/reading-and-writing-csvs-in-java/ 
     * credit: https://www.delftstack.com/howto/java/java-relative-path/
     * 
     * @return JFreeChart chart
     */
    private JFreeChart createChartPanel() throws IOException {

        OTTemp = createSeries(".\\CSVs\\OttawaOfficial.csv", "Ottawa Temp", 1);
        CATemp = createSeries(".\\CSVs\\CalgaryOfficial.csv", "Calgary Temp", 1);
        MOTemp = createSeries(".\\CSVs\\MonctonOfficial.csv", "Moncton Temp", 1);  
        OTPrec = createSeries(".\\CSVs\\OttawaOfficial.csv", "Ottawa Prec", 2);
        CAPrec = createSeries(".\\CSVs\\CalgaryOfficial.csv", "Calgary Prec", 2);
        MOPrec = createSeries(".\\CSVs\\MonctonOfficial.csv", "Moncton Prec", 2);

        dataset = new TimeSeriesCollection();
        dataset.addSeries(OTTemp);
        dataset.addSeries(CATemp);
        dataset.addSeries(MOTemp);    
        dataset.addSeries(OTPrec);
        dataset.addSeries(CAPrec);
        dataset.addSeries(MOPrec);

        JFreeChart chart = ChartFactory.createTimeSeriesChart(
                "Temperature and Precipitation in Canada", // Chart  
                "Date", // X-Axis Label  
                "Number", // Y-Axis Label  
                dataset);

        return chart;
    }
    
    /**
     *sets the series name and readds the series to preserve order
     * @param name the desired name
     */
    public void seriesname1(String name){
        TimeSeries temp = createSeries(".\\CSVs\\OttawaOfficial.csv", name, 1);
        dataset.removeAllSeries();
        dataset.addSeries(temp);
        dataset.addSeries(CATemp);
        dataset.addSeries(MOTemp);
        dataset.addSeries(OTPrec);
        dataset.addSeries(CAPrec);
        dataset.addSeries(MOPrec); 
        
    }
    /**
     *sets the series name and readds the series to preserve order
     * @param name the desired new name
     */
    public void seriesname2(String name){
        TimeSeries temp = createSeries(".\\CSVs\\CalgaryOfficial.csv", name, 1);
        dataset.removeAllSeries();
        dataset.addSeries(OTTemp);
        dataset.addSeries(temp);
        dataset.addSeries(MOTemp);
        dataset.addSeries(OTPrec);
        dataset.addSeries(CAPrec);
        dataset.addSeries(MOPrec); 
    }
    /**
     *sets the series name and readds the series to preserve order
     * @param name the desired new name
     */
    public void seriesname3(String name){
        TimeSeries temp = createSeries(".\\CSVs\\MonctonOfficial.csv", name, 1);
        dataset.removeAllSeries();
        dataset.addSeries(OTTemp);
        dataset.addSeries(CATemp);
        dataset.addSeries(temp);
        dataset.addSeries(OTPrec);
        dataset.addSeries(CAPrec);
        dataset.addSeries(MOPrec); 
    }
    /**
     *sets the series name and readds the series to preserve order
     * @param name the desired new name
     */
    public void seriesname4(String name){
        TimeSeries temp = createSeries(".\\CSVs\\OttawaOfficial.csv", name, 2);
        dataset.removeAllSeries();
        dataset.addSeries(OTTemp);
        dataset.addSeries(CATemp);
        dataset.addSeries(MOTemp);
        dataset.addSeries(temp);
        dataset.addSeries(CAPrec);
        dataset.addSeries(MOPrec); 
    }
    /**
     *sets the series name and readds the series to preserve order
     * @param name the desired new name
     */
    public void seriesname5(String name){
        TimeSeries temp = createSeries(".\\CSVs\\CalgaryOfficial.csv", name, 2);
        dataset.removeAllSeries();
        dataset.addSeries(OTTemp);
        dataset.addSeries(CATemp);
        dataset.addSeries(MOTemp);
        dataset.addSeries(OTPrec);
        dataset.addSeries(temp);
        dataset.addSeries(MOPrec); 
    }
    /**
     *sets the series name and readds the series to preserve order
     * @param name the desired new name
     */
    public void seriesname6(String name){
        TimeSeries temp = createSeries(".\\CSVs\\MonctonOfficial.csv", name, 2);
        dataset.removeAllSeries();
        dataset.addSeries(OTTemp);
        dataset.addSeries(CATemp);
        dataset.addSeries(MOTemp);
        dataset.addSeries(OTPrec);
        dataset.addSeries(CAPrec);
        dataset.addSeries(temp);
    }

    /**
     *a method that creates a TimeSeries from a CSV file
     * credit: https://stackabuse.com/reading-and-writing-csvs-in-java/
     * credit: https://www.javatpoint.com/java-string-to-int
     * @return TimeSeries series 
     */
    private TimeSeries createSeries(String filename, String seriesname, int column) {
        //make a time series
        TimeSeries series = new TimeSeries(seriesname);
        //parse the csv
        try ( BufferedReader csvReader = new BufferedReader(new FileReader(filename))) {
            String row;
            String headerLine = csvReader.readLine();
            //while there is a next line
            while ((row = csvReader.readLine()) != null) {
                //split the rows with ,
                String[] total = row.split(",");
                //create a double of the column with the information 
                double temp = Double.parseDouble(total[column]);
                //create a string array of the first column
                String[] date = row.split("/");
                //add the day and month as an int to the date, year is always 1940 to start
                String month = date[0];
                String day = date[1];
                //add it to the series
                series.add(new Day(Integer.parseInt(day), 
                        Integer.parseInt(month), 1940), temp);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Chart.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Chart.class.getName()).log(Level.SEVERE, null, ex);
        }
        return series;
    }

    /**
     *a method that changes the background color of the plot
     * @param plot the XYPlot to change 
     * @param color the Color to change it to
     */
    public void changeBackgroundColor(XYPlot plot, Color color) {
        plot.setBackgroundPaint(color);
    }

    /**
     *a getter for the JFreeChart
     * @return JFreeChart finalChart
     */
    public JFreeChart getJFreeChart() {
        return finalChart;
    }

    /**
     *a getter method for the XYPlot
     * @return XYPlot plot
     */
    public XYPlot getPlot() {
        return plot;
    }

     /**
     *a getter method for the renderer
     * @return XYLineRenderer renderer
     */
    public XYLineAndShapeRenderer getRenderer() {
        return renderer;
    }

    /**
     *reverts the series information to the original state
     * @throws IOException
     */
    public void revert() throws IOException {
        OTTemp = createSeries(".\\CSVs\\OttawaOfficial.csv", "Ottawa Temp", 1);
        CATemp = createSeries(".\\CSVs\\CalgaryOfficial.csv", "Calgary Temp", 1);
        MOTemp = createSeries(".\\CSVs\\MonctonOfficial.csv", "Moncton Temp", 1);  
        OTPrec = createSeries(".\\CSVs\\OttawaOfficial.csv", "Ottawa Prec", 2);
        CAPrec = createSeries(".\\CSVs\\CalgaryOfficial.csv", "Calgary Prec", 2);
        MOPrec = createSeries(".\\CSVs\\MonctonOfficial.csv", "Moncton Prec", 2);
        dataset.removeAllSeries();
        dataset.addSeries(OTTemp);
        dataset.addSeries(CATemp);
        dataset.addSeries(MOTemp);    
        dataset.addSeries(OTPrec);
        dataset.addSeries(CAPrec);
        dataset.addSeries(MOPrec);
        //adds the renderer to the plot
        plot.setRenderer(renderer);
    }
}
