package masino_3;

//imports
import com.sun.tools.javac.Main;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.jfree.chart.ChartPanel;

/**
 * creates a gui for the chart to be displayed and edited, largely basseed off 
 * of class example
 * @author zaramasino
 */
public class Masino_3 extends javax.swing.JFrame {

    /**
     * Creates new Masino_3
     *
     * @throws IOException
     */
    public Masino_3() throws IOException {
        initComponents();
        RenderChart();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     */
    @SuppressWarnings("unchecked")
    private void initComponents() throws IOException {
        // initialize variables
        pnl_sidebar = new javax.swing.JPanel();
        seriesShownLabel = new javax.swing.JLabel();
        range = new javax.swing.JLabel();
        Series1 = new javax.swing.JCheckBox();
        Series2 = new javax.swing.JCheckBox();
        Series3 = new javax.swing.JCheckBox();
        Series4 = new javax.swing.JCheckBox();
        Series5 = new javax.swing.JCheckBox();
        Series6 = new javax.swing.JCheckBox();
        domain = new javax.swing.JLabel();
        rangelow = new javax.swing.JLabel();
        rangehigh = new javax.swing.JLabel();
        domainlow = new javax.swing.JLabel();
        domainhigh = new javax.swing.JLabel();
        rangelowt = new javax.swing.JTextField();
        rangehight = new javax.swing.JTextField();
        domainlowt = new javax.swing.JTextField();
        domainhight = new javax.swing.JTextField();
        backgroundcolor = new javax.swing.JButton();
        pnl_chart = new javax.swing.JPanel();
        series_panel = new javax.swing.JPanel();
        pnl_sidebar1 = new javax.swing.JPanel();
        ylabel = new javax.swing.JLabel();
        xlabel = new javax.swing.JLabel();
        titlelabel = new javax.swing.JLabel();
        typelabel = new javax.swing.JLabel();
        colorlabel = new javax.swing.JLabel();
        namelabel = new javax.swing.JLabel();
        line2ComboBox = new javax.swing.JComboBox<>();
        save = new javax.swing.JButton();
        revert = new javax.swing.JButton();
        color1 = new javax.swing.JButton();
        series2name = new javax.swing.JTextField();
        series1name = new javax.swing.JTextField();
        series3name = new javax.swing.JTextField();
        series4name = new javax.swing.JTextField();
        series5name = new javax.swing.JTextField();
        series6name = new javax.swing.JTextField();
        color2 = new javax.swing.JButton();
        color3 = new javax.swing.JButton();
        color4 = new javax.swing.JButton();
        color5 = new javax.swing.JButton();
        color6 = new javax.swing.JButton();
        titlename = new javax.swing.JTextField();
        yname = new javax.swing.JTextField();
        xname = new javax.swing.JTextField();
        type1 = new javax.swing.JComboBox<>();
        type2 = new javax.swing.JComboBox<>();
        type3 = new javax.swing.JComboBox<>();
        type4 = new javax.swing.JComboBox<>();
        type5 = new javax.swing.JComboBox<>();
        type6 = new javax.swing.JComboBox<>();
        iconlabel = new javax.swing.JLabel();
        icon1 = new javax.swing.JComboBox<>();
        icon2 = new javax.swing.JComboBox<>();
        icon3 = new javax.swing.JComboBox<>();
        icon4 = new javax.swing.JComboBox<>();
        icon5 = new javax.swing.JComboBox<>();
        icon6 = new javax.swing.JComboBox<>();
        gridcolor = new javax.swing.JButton();
        Quit = new javax.swing.JLabel();
        c = new Chart();
        //create new editchart object
        custom = new EditChart(c);
        //create new chartpanel using the jfreechart of c
        cp = new ChartPanel(c.getJFreeChart());
        //set the background color of cp
        cp.setBackground(new Color(54, 63, 73));

        //set up close operation
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        //set background color
        setBackground(new java.awt.Color(255, 255, 255));
        setLocationByPlatform(true);
        setUndecorated(true);

        //set the background color
        pnl_sidebar.setBackground(new java.awt.Color(34, 41, 50));
        //add a mousemotionlistener
        pnl_sidebar.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            @Override
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                pnl_sidebarMouseDragged(evt);
            }
        });
        //add a mouselistener
        pnl_sidebar.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mousePressed(java.awt.event.MouseEvent evt) {
                pnl_sidebarMousePressed(evt);
            }
        });

        //set font, foreground, icon, and text for the sereisShownLabel
        seriesShownLabel.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        seriesShownLabel.setForeground(new java.awt.Color(166, 166, 166));
        seriesShownLabel.setIcon(new javax.swing.ImageIcon(getClass()
                .getResource("/project16/Icons/clock_18px.png"))); // NOI18N
        seriesShownLabel.setText("Series Shown");

        //set the fond, foreground, icon, and text for the range label
        range.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        range.setForeground(new java.awt.Color(166, 166, 166));
        range.setIcon(new javax.swing.ImageIcon(getClass()
                .getResource("/project16/Icons/search_18px_2.png"))); // NOI18N
        range.setText("Range");

        //sets the text
        Series3.setText("Moncton Temperature");
        //adds an action listener
        Series3.addActionListener((java.awt.event.ActionEvent evt) -> {
            Series3ActionPerformed(evt);
        });

        //sets the text
        Series5.setText("Ottawa Temperature");
        //adds an action listener
        Series5.addActionListener((java.awt.event.ActionEvent evt) -> {
            Series5ActionPerformed(evt);
        });

        //sets the text
        Series4.setText("Ottawa Precipitation");
        //adds an action listener
        Series4.addActionListener((java.awt.event.ActionEvent evt) -> {
            Series4ActionPerformed(evt);
        });
        //sets text
        Series1.setText("Ottawa Temperature");
        //adds and action listener
        Series1.addActionListener((java.awt.event.ActionEvent evt) -> {
            Series1ActionPerformed(evt);
        });

        //sets text
        Series2.setText("Calgary Temperature");
        //adds an action listener
        Series2.addActionListener((java.awt.event.ActionEvent evt) -> {
            Series2ActionPerformed(evt);
        });

        //sets text
        Series6.setText("Moncton Precipitation");
        //adds and action listener
        Series6.addActionListener((java.awt.event.ActionEvent evt) -> {
            Series6ActionPerformed(evt);
        });

        //sets all the checkboxe defaults to true
        //credit: https://www.codejava.net/java-se/swing/jcheckbox-basic-tutorial-and-examples 
        Series1.setSelected(true);
        Series2.setSelected(true);
        Series3.setSelected(true);
        Series4.setSelected(true);
        Series5.setSelected(true);
        Series6.setSelected(true);

        //sets the font, foreground, icon, and text for domain
        domain.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        domain.setForeground(new java.awt.Color(166, 166, 166));
        domain.setIcon(new javax.swing.ImageIcon(getClass()
                .getResource("/project16/Icons/search_18px_2.png"))); // NOI18N
        domain.setText("Domain");

        //sets the font, foreground, icon, and text for rangelow
        rangelow.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        rangelow.setForeground(new java.awt.Color(166, 166, 166));
        rangelow.setIcon(new javax.swing.ImageIcon(getClass()
                .getResource("/project16/Icons/search_18px_2.png"))); // NOI18N
        rangelow.setText("Low");

        //sets the font, foreground, icon, and text for rangehigh
        rangehigh.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        rangehigh.setForeground(new java.awt.Color(166, 166, 166));
        rangehigh.setIcon(new javax.swing.ImageIcon(getClass()
                .getResource("/project16/Icons/search_18px_2.png"))); // NOI18N
        rangehigh.setText("High");

        //sets the font, foreground, icon, and text for domainlow
        domainlow.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        domainlow.setForeground(new java.awt.Color(166, 166, 166));
        domainlow.setIcon(new javax.swing.ImageIcon(getClass()
                .getResource("/project16/Icons/search_18px_2.png"))); // NOI18N
        domainlow.setText("Low");

        //sets the font, foreground, icon, and text for domainhigh
        domainhigh.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        domainhigh.setForeground(new java.awt.Color(166, 166, 166));
        domainhigh.setIcon(new javax.swing.ImageIcon(getClass()
                .getResource("/project16/Icons/search_18px_2.png"))); // NOI18N
        domainhigh.setText("High");

        //sets the default text to the lower range of custom
        rangelowt.setText(custom.getlowerRange());
        //adds action listener
        rangelowt.addActionListener((ActionEvent e) -> {
            //sets the lower range of custom to the text converted to an int
            //credit:  https://www.javatpoint.com/java-string-to-int 
            custom.setlowerRange(Integer.parseInt(rangelowt.getText()));
        });

        //sets the default text to the upper range of custom
        //credit:  https://www.javatpoint.com/java-string-to-int 
        rangehight.setText(custom.getupperRange());
        //adds action listener
        rangehight.addActionListener((ActionEvent e) -> {
            //sets the upper range of custom to the text converted to an int
            custom.setupperRange(Integer.parseInt(rangehight.getText()));
        });


        // credit: https://stackoverflow.com/questions/18259644/how-to-check-if-a-string-matches-a-specific-format 
        domainlowt.setText(custom.getlowerDomain());
        domainlowt.addActionListener((ActionEvent e) -> {
            String check = domainlowt.getText();
            if (check.matches("\\d{2}/\\d{2}/\\d{4}")) {
                custom.setlowerDomain(domainlowt.getText());
            } else {
                JOptionPane.showMessageDialog(null, 
                        "Incorrect format. Please follow this format: mm/dd/yyyy", "", 
                        JOptionPane.INFORMATION_MESSAGE);
            }
        });

        // credit: https://stackoverflow.com/questions/18259644/how-to-check-if-a-string-matches-a-specific-format 
        domainhight.setText(custom.getupperDomain());
        domainhight.addActionListener((ActionEvent e) -> {
            String check = domainhight.getText();
            if (check.matches("\\d{2}/\\d{2}/\\d{4}")) {
                custom.setupperDomain(domainlowt.getText());
            } else {
                JOptionPane.showMessageDialog(null, 
                        "Incorrect format. Please follow this format: mm/dd/yyyy", "", 
                        JOptionPane.INFORMATION_MESSAGE);
            }
        });

        //sets the text of backgroundcolor
        backgroundcolor.setText("Change Background Color");
        //adds action listener
        backgroundcolor.addActionListener((ActionEvent e) -> {
            // color is the Color returned from  a JColorChooser
            //credit: https://stackoverflow.com/questions/65709269/add-a-color-selector-to-a-panel-in-java
            Color color = JColorChooser.showDialog(series_panel,
                    "Make a choice", Color.MAGENTA);
            //if a color is chosen
            if (color != null) {
                //call the changebackgroundcolor method with color
                custom.changeBackgroundColor(color);
            }
        });

        //generated code from the Design feature, formatting and spacing blah blah no need to look here
        javax.swing.GroupLayout pnl_sidebarLayout = new javax.swing.GroupLayout(pnl_sidebar);
        pnl_sidebar.setLayout(pnl_sidebarLayout);
        pnl_sidebarLayout.setHorizontalGroup(
                pnl_sidebarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnl_sidebarLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(pnl_sidebarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(pnl_sidebarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(Series6, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnl_sidebarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(Series5, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnl_sidebarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                .addComponent(Series2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(Series3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(Series4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addGroup(pnl_sidebarLayout.createSequentialGroup()
                                                                        .addGroup(pnl_sidebarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                                                .addComponent(domain, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnl_sidebarLayout.createSequentialGroup()
                                                                                        .addGap(20, 20, 20)
                                                                                        .addGroup(pnl_sidebarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                                .addComponent(rangelow, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                .addComponent(rangelowt, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                                        .addGap(0, 0, Short.MAX_VALUE)))
                                                                        .addGap(12, 12, 12)))
                                                        .addComponent(Series1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addComponent(range, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(seriesShownLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(pnl_sidebarLayout.createSequentialGroup()
                                                .addGap(10, 10, 10)
                                                .addGroup(pnl_sidebarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(backgroundcolor, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(pnl_sidebarLayout.createSequentialGroup()
                                                                .addGroup(pnl_sidebarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                        .addComponent(domainlowt, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(domainlow, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addGap(40, 40, 40)
                                                                .addGroup(pnl_sidebarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(domainhight, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(domainhigh, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(rangehight, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(rangehigh, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                                .addContainerGap(22, Short.MAX_VALUE))
        );
        pnl_sidebarLayout.setVerticalGroup(
                pnl_sidebarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnl_sidebarLayout.createSequentialGroup()
                                .addGap(38, 38, 38)
                                .addComponent(seriesShownLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(Series1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(Series2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(Series3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(Series4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(Series5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(Series6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                                .addComponent(range, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnl_sidebarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(rangehigh)
                                        .addComponent(rangelow, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnl_sidebarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(rangehight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(rangelowt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(domain, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnl_sidebarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(domainlow)
                                        .addComponent(domainhigh))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnl_sidebarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(domainlowt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(domainhight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(backgroundcolor)
                                .addGap(22, 22, 22))
        );

        //set the background color of the panel chart
        pnl_chart.setBackground(new java.awt.Color(54, 63, 73));
        //sets the panel layout to borderlayout 
        pnl_chart.setLayout(new java.awt.BorderLayout());
        //set the background color of series panel
        series_panel.setBackground(new java.awt.Color(255, 255, 255));
        //set the series panel layout to borderlayout
        series_panel.setLayout(new javax.swing.BoxLayout(series_panel, 
                javax.swing.BoxLayout.LINE_AXIS));
        //set the background color of the side panel
        pnl_sidebar1.setBackground(new java.awt.Color(34, 41, 50));
        //sets the background color, font, foreground, icon, text, and tooltip for ylabel
        ylabel.setBackground(new java.awt.Color(48, 201, 235));
        ylabel.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ylabel.setForeground(new java.awt.Color(48, 201, 235));
        ylabel.setIcon(new javax.swing.ImageIcon(getClass()
                .getResource("/project16/Icons/home_18px.png"))); // NOI18N
        ylabel.setText("Y Axis Title");
        ylabel.setToolTipText("");

        //sets the font, foreground, icon, and text for namelabel
        namelabel.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        namelabel.setForeground(new java.awt.Color(166, 166, 166));
        namelabel.setIcon(new javax.swing.ImageIcon(getClass()
                .getResource("/project16/Icons/plus_18px_1.png"))); // NOI18N
        namelabel.setText("Name");

        //sets the font, foreground, icon, and text for typelabel
        typelabel.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        typelabel.setForeground(new java.awt.Color(166, 166, 166));
        typelabel.setIcon(new javax.swing.ImageIcon(getClass()
                .getResource("/project16/Icons/plus_18px_1.png"))); // NOI18N
        typelabel.setText("Line Type");

        //sets the font, foreground, icon, and text for colorlabel
        colorlabel.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        colorlabel.setForeground(new java.awt.Color(166, 166, 166));
        colorlabel.setIcon(new javax.swing.ImageIcon(getClass()
                .getResource("/project16/Icons/plus_18px_1.png"))); // NOI18N
        colorlabel.setText("Line Color");

        //sets the background, font, foreground, icon, and text for titlelabel
        titlelabel.setBackground(new java.awt.Color(48, 201, 235));
        titlelabel.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        titlelabel.setForeground(new java.awt.Color(48, 201, 235));
        titlelabel.setIcon(new javax.swing.ImageIcon(getClass()
                .getResource("/project16/Icons/home_18px.png"))); // NOI18N
        titlelabel.setText("Title");
        titlelabel.setToolTipText("");

        //sets the font, foreground, icon, text, and tooltip for xlabel
        xlabel.setBackground(new java.awt.Color(48, 201, 235));
        xlabel.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        xlabel.setForeground(new java.awt.Color(48, 201, 235));
        xlabel.setIcon(new javax.swing.ImageIcon(getClass()
                .getResource("/project16/Icons/home_18px.png"))); // NOI18N
        xlabel.setText("X Axis Title");
        xlabel.setToolTipText("");

        //set save text
        save.setText("Save");

        //set series1name text
        series1name.setText("Ottawa Temperature");
        //add action listener
        series1name.addActionListener((ActionEvent event) -> {
            //set newname to the text of the textfield
            String newname = series1name.getText();
            //remake the dataset for that series
            c.seriesname1(newname);
            //set the appropriate checkbox text to match
            Series1.setText(newname);
        });
        //set series2name text
        series2name.setText("Calgary Temperature");
        //add action listener
        series2name.addActionListener((ActionEvent event) -> {
           //set newname to the text of the textfield
            String newname = series2name.getText();
            //remake the dataset for that series
            c.seriesname2(newname);
            //set the appropriate checkbox text to match
            Series2.setText(newname);
        });
        //set series3name text
        series3name.setText("Moncton Temperature");
        //add action listener
        series3name.addActionListener((ActionEvent event) -> {
            //set newname to the text of the textfield
            String newname = series3name.getText();
            //remake the dataset for that series
            c.seriesname3(newname);
            //set the appropriate checkbox text to match
            Series3.setText(newname);
        });
        //set series4name text
        series4name.setText("Ottawa Precipitation");
        //add action listener
        series4name.addActionListener((ActionEvent event) -> {
            //set newname to the text of the textfield
            String newname = series4name.getText();
            //remake the dataset for that series
            c.seriesname4(newname);
            //set the appropriate checkbox text to match
            Series4.setText(newname);
        });
        //set series5name text
        series5name.setText("Calgary Precipitation");
        //add action listener
        series5name.addActionListener((ActionEvent event) -> {
            //set newname to the text of the textfield
            String newname = series5name.getText();
            //remake the dataset for that series
            c.seriesname5(newname);
            //set the appropriate checkbox text to match
            Series5.setText(newname);
        });
        //set series6name text
        series6name.setText("Moncton Precipitation");
        //add action listener
        series6name.addActionListener((ActionEvent event) -> {
            //set newname to the text of the textfield
            String newname = series6name.getText();
            //remake the dataset for that series
            c.seriesname6(newname);
            //set the appropriate checkbox text to match
            Series6.setText(newname);
        });

        //set color1 text
        color1.setText("Color");
        //add action listener
        color1.addActionListener((ActionEvent e) -> {
            //color from a JColorChooser
            //credit: https://stackoverflow.com/questions/65709269/add-a-color-selector-to-a-panel-in-java
            Color color = JColorChooser.showDialog(series_panel,
                    "Make a choice", Color.MAGENTA);
            if (color != null) {
                //set the series color to color
                custom.changeSeriesColor(0, color);
            }
        });

        //set color2 text
        color2.setText("Color");
        //add action listener
        color2.addActionListener((ActionEvent e) -> {
            //color from a JColorChooser
            //credit: https://stackoverflow.com/questions/65709269/add-a-color-selector-to-a-panel-in-java
            Color color = JColorChooser.showDialog(series_panel,
                    "Make a choice", Color.MAGENTA);
            if (color != null) {
                //set the series color to color
                custom.changeSeriesColor(1, color);
            }
        });

        //set color3 text
        color3.setText("Color");
        //add action listener
        color3.addActionListener((ActionEvent e) -> {
            //color from a JColorChooser
            //credit: https://stackoverflow.com/questions/65709269/add-a-color-selector-to-a-panel-in-java
            Color color = JColorChooser.showDialog(series_panel,
                    "Make a choice", Color.MAGENTA);
            if (color != null) {
                //set the series color to color
                custom.changeSeriesColor(2, color);
            }
        });

        //set color4 text
        color4.setText("Color");
        //add action listener
        color4.addActionListener((ActionEvent e) -> {
            //color from a JColorChooser
            //credit: https://stackoverflow.com/questions/65709269/add-a-color-selector-to-a-panel-in-java
            Color color = JColorChooser.showDialog(series_panel,
                    "Make a choice", Color.MAGENTA);
            if (color != null) {
                //set the series color to color
                custom.changeSeriesColor(3, color);
            }
        });

        //set color5 text
        color5.setText("Color");
        //add action listener
        color5.addActionListener((ActionEvent e) -> {
            //color from a JColorChooser
            //credit: https://stackoverflow.com/questions/65709269/add-a-color-selector-to-a-panel-in-java
            Color color = JColorChooser.showDialog(series_panel,
                    "Make a choice", Color.MAGENTA);
            if (color != null) {
                //set the series color to color
                custom.changeSeriesColor(4, color);
            }
        });

        //set color6 text
        color6.setText("Color");
        //add action listener
        color6.addActionListener((ActionEvent e) -> {
            //color from a JColorChooser
            //credit: https://stackoverflow.com/questions/65709269/add-a-color-selector-to-a-panel-in-java
            Color color = JColorChooser.showDialog(series_panel,
                    "Make a choice", Color.MAGENTA);
            if (color != null) {
                //set the series color to color
                custom.changeSeriesColor(5, color);
            }
        });

        //set the title text to the title
        titlename.setText(custom.getTitle());
        //add action listener
        titlename.addActionListener((java.awt.event.ActionEvent evt) -> {
            titlenameActionPerformed(evt);
        });

        //set the yname text
        yname.setText("Number");
        //add action listener
        yname.addActionListener((java.awt.event.ActionEvent evt) -> {
            ynameActionPerformed(evt);
        });

        //set the xname text
        xname.setText("Date");
        //add action listener
        xname.addActionListener((java.awt.event.ActionEvent evt) -> {
            xnameActionPerformed(evt);
        });

        //set the model to have solid and dashed
        //line types extra credit?
        type1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]
        {"Solid", "Dashed"}));
        //add action listener
        type1.addItemListener((ItemEvent event) -> {
            //if the combobox changes 
            if (event.getStateChange() == ItemEvent.SELECTED) {
                //get the index of the current option
                int index = type1.getSelectedIndex();
                //switch calls solidline if index is 0, dashedline if index is 1
                switch (index) {
                    case 0 ->
                        custom.solidLine(0);
                    case 1 ->
                        custom.dashedLine(0);
                    default -> {
                    }
                }
            }
        });
        //set the model to have solid and dashed
        type2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]
        {"Solid", "Dashed"}));
        //add action listener
        type2.addItemListener((ItemEvent event) -> {
            //if the combobox changes 
            if (event.getStateChange() == ItemEvent.SELECTED) {
                //get the index of the current option
                int index = type2.getSelectedIndex();
                //switch calls solidline if index is 0, dashedline if index is 1
                switch (index) {
                    case 0 ->
                        custom.solidLine(1);
                    case 1 ->
                        custom.dashedLine(1);
                    default -> {
                    }
                }
            }
        });
        //set the model to have solid and dashed
        type3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]
        {"Solid", "Dashed"}));
        //add action listener
        type3.addItemListener((ItemEvent event) -> {
            //if the combobox changes 
            if (event.getStateChange() == ItemEvent.SELECTED) {
                //get the index of the current option
                int index = type3.getSelectedIndex();
                //switch calls solidline if index is 0, dashedline if index is 1
                switch (index) {
                    case 0 ->
                        custom.solidLine(2);
                    case 1 ->
                        custom.dashedLine(2);
                    default -> {
                    }
                }
            }
        });
        //set the model to have solid and dashed
        type4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]
        {"Solid", "Dashed"}));
        //add action listener
        type4.addItemListener((ItemEvent event) -> {
             //if the combobox changes 
            if (event.getStateChange() == ItemEvent.SELECTED) {
                //get the index of the current option
                int index = type4.getSelectedIndex();
                //switch calls solidline if index is 0, dashedline if index is 1
                switch (index) {
                    case 0 ->
                        custom.solidLine(3);
                    case 1 ->
                        custom.dashedLine(3);
                    default -> {
                    }
                }
            }
        });
        //set the model to have solid and dashed
        type5.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]
        {"Solid", "Dashed"}));
        //add action listener
        type5.addItemListener((ItemEvent event) -> {
            //if the combobox changes 
            if (event.getStateChange() == ItemEvent.SELECTED) {
                //get the index of the current option
                int index = type5.getSelectedIndex();
                //switch calls solidline if index is 0, dashedline if index is 1
                switch (index) {
                    case 0 ->
                        custom.solidLine(4);
                    case 1 ->
                        custom.dashedLine(4);
                    default -> {
                    }
                }
            }
        });
        //set the model to have solid and dashed
        type6.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]
        {"Solid", "Dashed"}));
        //add action listener
        type6.addItemListener((ItemEvent event) -> {
            //if the combobox changes 
            if (event.getStateChange() == ItemEvent.SELECTED) {
                //get the index of the current option
                int index = type6.getSelectedIndex();
                //switch calls solidline if index is 0, dashedline if index is 1
                switch (index) {
                    case 0 ->
                        custom.solidLine(5);
                    case 1 ->
                        custom.dashedLine(5);
                    default -> {
                    }
                }
            }
        });
        //sets the font, foreground, icon, and text for iconlabel
        iconlabel.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        iconlabel.setForeground(new java.awt.Color(166, 166, 166));
        iconlabel.setIcon(new javax.swing.ImageIcon(getClass()
                .getResource("/project16/Icons/plus_18px_1.png"))); // NOI18N
        iconlabel.setText("Point Icon");

        //icons extra credit?
        icon1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]
        {"Square", "Circle", "Diamond", "Cross"}));
        //add action listener
        icon1.addItemListener((ItemEvent event) -> {
            //if the combobox changes
            if (event.getStateChange() == ItemEvent.SELECTED) {
                //get the index of the current option
                int index = icon1.getSelectedIndex();
                //switch calls squareicon if index is 0, circleicon if index 
                //is 1, diamondicon if index is 2, or crossicon if index is 3
                switch (index) {
                    case 0 ->
                        custom.squareIcon(0);
                    case 1 ->
                        custom.circleIcon(0);
                    case 2 ->
                        custom.diamondIcon(0);
                    case 3 ->
                        custom.crossIcon(0);
                    default -> {
                    }
                }
            }
        });

        icon2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]
        {"Square", "Circle", "Diamond", "Cross"}));
        //add action listener
        icon2.addItemListener((ItemEvent event) -> {
             //if the combobox changes
            if (event.getStateChange() == ItemEvent.SELECTED) {
                //get the index of the current option
                int index = icon2.getSelectedIndex();
                //switch calls squareicon if index is 0, circleicon if index 
                //is 1, diamondicon if index is 2, or crossicon if index is 3
                switch (index) {
                    case 0 ->
                        custom.squareIcon(1);
                    case 1 ->
                        custom.circleIcon(1);
                    case 2 ->
                        custom.diamondIcon(1);
                    case 3 ->
                        custom.crossIcon(1);
                    default -> {
                    }
                }
            }
        });

        icon3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]
        {"Square", "Circle", "Diamond", "Cross"}));
        //add action listener
        icon3.addItemListener((ItemEvent event) -> {
            //if the combobox changes
            if (event.getStateChange() == ItemEvent.SELECTED) {
                //get the index of the current option
                int index = icon3.getSelectedIndex();
                //switch calls squareicon if index is 0, circleicon if index 
                //is 1, diamondicon if index is 2, or crossicon if index is 3
                switch (index) {
                    case 0 ->
                        custom.squareIcon(2);
                    case 1 ->
                        custom.circleIcon(2);
                    case 2 ->
                        custom.diamondIcon(2);
                    case 3 ->
                        custom.crossIcon(2);
                    default -> {
                    }
                }
            }
        });

        icon4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]
        {"Square", "Circle", "Diamond", "Cross"}));
        //add action listener
        icon4.addItemListener((ItemEvent event) -> {
             //if the combobox changes
            if (event.getStateChange() == ItemEvent.SELECTED) {
                //get the index of the current option
                int index = icon4.getSelectedIndex();
                //switch calls squareicon if index is 0, circleicon if index is
                //1, diamondicon if index is 2, or crossicon if index is 3
                switch (index) {
                    case 0 ->
                        custom.squareIcon(3);
                    case 1 ->
                        custom.circleIcon(3);
                    case 2 ->
                        custom.diamondIcon(3);
                    case 3 ->
                        custom.crossIcon(3);
                    default -> {
                    }
                }
            }
        });

        icon5.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]
        {"Square", "Circle", "Diamond", "Cross"}));
        //add action listener
        icon5.addItemListener((ItemEvent event) -> {
             //if the combobox changes
            if (event.getStateChange() == ItemEvent.SELECTED) {
                //get the index of the current option
                int index = icon5.getSelectedIndex();
                //switch calls squareicon if index is 0, circleicon if index 
                //is 1, diamondicon if index is 2, or crossicon if index is 3
                switch (index) {
                    case 0 ->
                        custom.squareIcon(4);
                    case 1 ->
                        custom.circleIcon(4);
                    case 2 ->
                        custom.diamondIcon(4);
                    case 3 ->
                        custom.crossIcon(4);
                    default -> {
                    }
                }
            }
        });

        icon6.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]
        {"Square", "Circle", "Diamond", "Cross"}));
        //add action listener
        icon6.addItemListener((ItemEvent event) -> {
             //if the combobox changes
            if (event.getStateChange() == ItemEvent.SELECTED) {
                //get the index of the current option
                int index = icon6.getSelectedIndex();
                //switch calls squareicon if index is 0, circleicon if index 
                //is 1, diamondicon if index is 2, or crossicon if index is 3
                switch (index) {
                    case 0 ->
                        custom.squareIcon(5);
                    case 1 ->
                        custom.circleIcon(5);
                    case 2 ->
                        custom.diamondIcon(5);
                    case 3 ->
                        custom.crossIcon(5);
                    default -> {
                    }
                }
            }
        });

        //set gridcolor text
        gridcolor.setText("GridColor");
        //add action listener
        gridcolor.addActionListener((ActionEvent e) -> {
            //color from JColorChooser
            //credit: https://stackoverflow.com/questions/65709269/add-a-color-selector-to-a-panel-in-java
            Color color = JColorChooser.showDialog(series_panel,
                    "Make a choice", Color.MAGENTA);
            if (color != null) {
                //call changegridcolor with color
                custom.changeGridColor(color);
            }
        });

        //add action listener to save
        save.addActionListener((ActionEvent arg0) -> {
            // Try catch block to ensure null files do not interfere
            try {
                // Calls the saved method
                saved();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        //set text of button
        revert.setText("Revert");
        //add action listener
        revert.addActionListener((ActionEvent evt) -> {
            try {
                revert();
            } catch (IOException ex) {
                Logger.getLogger(Masino_3.class.getName()).log(Level.SEVERE, null, ex);
            }
        });


        //generated code, carry on
        javax.swing.GroupLayout pnl_sidebar1Layout = new javax.swing.GroupLayout(pnl_sidebar1);
        pnl_sidebar1.setLayout(pnl_sidebar1Layout);
        pnl_sidebar1Layout.setHorizontalGroup(
                pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_sidebar1Layout.createSequentialGroup()
                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(pnl_sidebar1Layout.createSequentialGroup()
                                                .addContainerGap()
                                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(pnl_sidebar1Layout.createSequentialGroup()
                                                                .addComponent(namelabel, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(14, 14, 14)
                                                                .addComponent(series1name, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(colorlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(pnl_sidebar1Layout.createSequentialGroup()
                                                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(typelabel, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(iconlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addGap(14, 14, 14)
                                                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(color1, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(type1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addComponent(icon1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                                                .addGap(18, 18, 18)
                                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                .addComponent(series2name)
                                                                .addComponent(type2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(icon2, 0, 121, Short.MAX_VALUE))
                                                        .addComponent(color2, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                .addComponent(series3name)
                                                                .addComponent(type3, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(icon3, 0, 121, Short.MAX_VALUE))
                                                        .addComponent(color3, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(pnl_sidebar1Layout.createSequentialGroup()
                                                                .addGap(18, 18, 18)
                                                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(series4name, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(type4, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(color4, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_sidebar1Layout.createSequentialGroup()
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(icon4, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(pnl_sidebar1Layout.createSequentialGroup()
                                                                .addGap(16, 16, 16)
                                                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(color5, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(icon5, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_sidebar1Layout.createSequentialGroup()
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(series5name, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(type5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                                .addGap(18, 18, 18)
                                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                                .addComponent(series6name, javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(type6, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(icon6, javax.swing.GroupLayout.Alignment.LEADING, 0, 121, Short.MAX_VALUE))
                                                        .addComponent(color6, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(gridcolor, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(titlelabel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(ylabel, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(xlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(pnl_sidebar1Layout.createSequentialGroup()
                                                .addGap(333, 333, 333)
                                                .addComponent(line2ComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(pnl_sidebar1Layout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(revert, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(save, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(yname)
                                        .addComponent(titlename, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(xname))
                                .addContainerGap())
        );
        pnl_sidebar1Layout.setVerticalGroup(
                pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnl_sidebar1Layout.createSequentialGroup()
                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(pnl_sidebar1Layout.createSequentialGroup()
                                                .addContainerGap()
                                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(namelabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(series2name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(series1name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(series3name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(series4name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(series5name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(series6name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(titlelabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(titlename, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(typelabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(type1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(type2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(type3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(type4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(type5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(type6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(ylabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(yname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                                .addComponent(iconlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(icon1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(icon2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(icon3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(icon4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                                .addComponent(icon6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(xlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(xname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(icon5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(pnl_sidebar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(colorlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(color1)
                                                        .addComponent(color2)
                                                        .addComponent(color3)
                                                        .addComponent(color4)
                                                        .addComponent(color5)
                                                        .addComponent(color6)
                                                        .addComponent(revert)
                                                        .addComponent(save)
                                                        .addComponent(gridcolor)))
                                        .addGroup(pnl_sidebar1Layout.createSequentialGroup()
                                                .addGap(341, 341, 341)
                                                .addComponent(line2ComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        //sets the background color, font, foreground, horizontal alignment, text, and opaque for quit
        Quit.setBackground(new java.awt.Color(34, 41, 50));
        Quit.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        Quit.setForeground(new java.awt.Color(255, 255, 255));
        Quit.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        Quit.setText("X  ");
        Quit.setOpaque(true);
        //add mouse listener
        Quit.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel1MousePressed(evt);
            }
        });

        //more generated code, nothing to see here
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(pnl_sidebar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Quit, javax.swing.GroupLayout.PREFERRED_SIZE, 1100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 4, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(pnl_sidebar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(211, 211, 211)
                                                .addComponent(pnl_chart, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(series_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(Quit, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(series_panel, javax.swing.GroupLayout.PREFERRED_SIZE, 396, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGap(37, 37, 37)
                                                                .addComponent(pnl_chart, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                        .addComponent(pnl_sidebar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                                .addComponent(pnl_sidebar1, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }
    int xx;
    int xy;

    private void pnl_sidebarMousePressed(java.awt.event.MouseEvent evt) {
        //record the x and y coordinates
        xx = evt.getX();
        xy = evt.getY();
    }

    private void pnl_sidebarMouseDragged(java.awt.event.MouseEvent evt) {
        //change the x and y to match, allows you to move the screen
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x - xx, y - xy);
    }

    private void jLabel1MousePressed(java.awt.event.MouseEvent evt) {
        //enable quit button on click
        System.exit(0);
    }

    
    private void Series3ActionPerformed(java.awt.event.ActionEvent evt) {
        //toggle series 3 visible and invisible
        JCheckBox cbLog = (JCheckBox) evt.getSource();
        if (cbLog.isSelected()) {
            custom.showSeries(2);
        } else {
            custom.hideSeries(2);
        }
    }

    private void Series1ActionPerformed(java.awt.event.ActionEvent evt) {
        //toggle series 1 visible and invisible
        JCheckBox cbLog = (JCheckBox) evt.getSource();
        if (cbLog.isSelected()) {
            custom.showSeries(0);
        } else {
            custom.hideSeries(0);
        }

    }

    private void Series2ActionPerformed(java.awt.event.ActionEvent evt) {
        //toggle series 2 visible and invisible
        JCheckBox cbLog = (JCheckBox) evt.getSource();
        if (cbLog.isSelected()) {
            custom.showSeries(1);
        } else {
            custom.hideSeries(1);
        }
    }

    private void Series4ActionPerformed(java.awt.event.ActionEvent evt) {
        //toggle series 4 visible and invisible
        JCheckBox cbLog = (JCheckBox) evt.getSource();
        if (cbLog.isSelected()) {
            custom.showSeries(3);
        } else {
            custom.hideSeries(3);
        }
    }

    private void Series5ActionPerformed(java.awt.event.ActionEvent evt) {
        //toggle series 5 visible and invisible
        JCheckBox cbLog = (JCheckBox) evt.getSource();
        if (cbLog.isSelected()) {
            custom.showSeries(4);
        } else {
            custom.hideSeries(4);
        }
    }

    private void Series6ActionPerformed(java.awt.event.ActionEvent evt) {
        //toggle series 6 visible and invisible
        JCheckBox cbLog = (JCheckBox) evt.getSource();
        if (cbLog.isSelected()) {
            custom.showSeries(5);
        } else {
            custom.hideSeries(5);
        }
    }

    private void xnameActionPerformed(java.awt.event.ActionEvent evt) {
        //call change xname with the text of the field
        custom.changeXName(xname.getText());
    }

    private void ynameActionPerformed(java.awt.event.ActionEvent evt) {
        //call change yname with the text of the field
        custom.changeYName(yname.getText());
    }

    private void titlenameActionPerformed(java.awt.event.ActionEvent evt) {
        //calls change title with the text of the field
        custom.changeTitle(titlename.getText());

    }

    //set the label color
    public void setLblColor(JLabel lbl) {
        lbl.setForeground(new Color(48, 201, 235));
    }

    //set the label color
    public void resetLblColor(JLabel lbl) {
        lbl.setForeground(new Color(166, 166, 166));
    }

    /*
    render the chart 
    */
    private void RenderChart() {
        //create a nwe chartpanel with the jfreechart of the c chart
        cp = new ChartPanel(c.getJFreeChart());
        //set the background color
        cp.setBackground(new Color(54, 63, 73));
        //add the chartpanel to the chart panel
        pnl_chart.add(cp, BorderLayout.CENTER);
        //validate the chart panel
        pnl_chart.validate();

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info :
                    javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | 
                IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Masino_3.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        //Create and display the form 
        java.awt.EventQueue.invokeLater(() -> {
            try {
                new Masino_3().setVisible(true);
            } catch (IOException ex) {
                Logger.getLogger(Masino_3.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        });
    }

    // Variables declaration                     
    private javax.swing.JCheckBox Series1;
    private javax.swing.JCheckBox Series2;
    private javax.swing.JCheckBox Series3;
    private javax.swing.JCheckBox Series4;
    private javax.swing.JCheckBox Series5;
    private javax.swing.JCheckBox Series6;
    private javax.swing.JButton backgroundcolor;
    private javax.swing.JButton color1;
    private javax.swing.JButton color2;
    private javax.swing.JButton color3;
    private javax.swing.JButton color4;
    private javax.swing.JButton color5;
    private javax.swing.JButton color6;
    private javax.swing.JLabel colorlabel;
    private javax.swing.JLabel domain;
    private javax.swing.JLabel domainhigh;
    private javax.swing.JTextField domainhight;
    private javax.swing.JLabel domainlow;
    private javax.swing.JTextField domainlowt;
    private javax.swing.JButton gridcolor;
    private javax.swing.JComboBox<String> icon1;
    private javax.swing.JComboBox<String> icon2;
    private javax.swing.JComboBox<String> icon3;
    private javax.swing.JComboBox<String> icon4;
    private javax.swing.JComboBox<String> icon5;
    private javax.swing.JComboBox<String> icon6;
    private javax.swing.JLabel iconlabel;
    private javax.swing.JLabel Quit;
    private javax.swing.JComboBox<String> line2ComboBox;
    private javax.swing.JLabel namelabel;
    private javax.swing.JPanel pnl_chart;
    private javax.swing.JPanel pnl_sidebar;
    private javax.swing.JPanel pnl_sidebar1;
    private javax.swing.JPanel series_panel;
    private javax.swing.JLabel range;
    private javax.swing.JLabel rangehigh;
    private javax.swing.JTextField rangehight;
    private javax.swing.JLabel rangelow;
    private javax.swing.JTextField rangelowt;
    private javax.swing.JButton save;
    private javax.swing.JButton revert;
    private javax.swing.JTextField series1name;
    private javax.swing.JTextField series2name;
    private javax.swing.JTextField series3name;
    private javax.swing.JTextField series4name;
    private javax.swing.JTextField series5name;
    private javax.swing.JTextField series6name;
    private javax.swing.JLabel seriesShownLabel;
    private javax.swing.JLabel titlelabel;
    private javax.swing.JTextField titlename;
    private javax.swing.JComboBox<String> type1;
    private javax.swing.JComboBox<String> type2;
    private javax.swing.JComboBox<String> type3;
    private javax.swing.JComboBox<String> type4;
    private javax.swing.JComboBox<String> type5;
    private javax.swing.JComboBox<String> type6;
    private javax.swing.JLabel typelabel;
    private javax.swing.JLabel xlabel;
    private javax.swing.JTextField xname;
    private javax.swing.JLabel ylabel;
    private javax.swing.JTextField yname;
    private Chart c;
    private EditChart custom;
    private ChartPanel cp;

    /*
    reverts the gui visual components to the default
    */
    public void revert() throws IOException{
        //call the revert functions on the chart and the editchart
        c.revert();
        custom.revert();
        icon1.setSelectedIndex(0);
        icon2.setSelectedIndex(0);
        icon3.setSelectedIndex(0);
        icon4.setSelectedIndex(0);
        icon5.setSelectedIndex(0);
        icon6.setSelectedIndex(0);
        type1.setSelectedIndex(0);
        type2.setSelectedIndex(0);
        type3.setSelectedIndex(0);
        type4.setSelectedIndex(0);
        type5.setSelectedIndex(0);
        type6.setSelectedIndex(0);
        yname.setText("Number");
        xname.setText("Date");
        titlename.setText(custom.getTitle());
        series1name.setText("Ottawa Temperature");
        series2name.setText("Calgary Temperature");
        series3name.setText("Moncton Temperature");
        series4name.setText("Ottawa Precipitation");
        series5name.setText("Calgary Precipitation");
        series6name.setText("Moncton Precipitation");
        Series1.setText("Ottawa Temperature");
        Series2.setText("Calgary Temperature");
        Series3.setText("Moncton Temperature");
        Series4.setText("Ottawa Precipitation");
        Series5.setText("Calgary Precipitation");
        Series6.setText("Moncton Precipitation");
    }
    
    // End of variables declaration      
    public void saved() throws IOException {
        // Create a new fileChooser
        JFileChooser fileChooser = new JFileChooser();
        // Filter the fileChooser with pngs
        fileChooser.setFileFilter(new FileNameExtensionFilter("*.png", "png"));
        // If the user selects the save option in the fileChooser
        if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            // Create a file of the selected file
            File file = fileChooser.getSelectedFile();
            // If that file already exists and is not a directory
            // Credit: https://stackoverflow.com/questions/1816673/how-do-i-check-if-a-file-exists-in-java
            if (file.exists() && !file.isDirectory()) {
                // Pop up a warning window that the old file will be overwritten
                // Credit: https://stackoverflow.com/questions/8581215/jfilechooser-and-checking-for-overwrite
                int response = JOptionPane.showConfirmDialog(this,
                        "The file " + file.getName()
                        + " already exists. Do you want to replace the existing file?",
                        "Ovewrite file", JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE);
                // If the user wants to continue and overwrite the file
                if (response == JOptionPane.YES_OPTION) {
                    // Save the current image to the computer
                    //Create a rectangle with the bound of the chart panel
                    Rectangle rec = cp.getBounds();
                    //make a new buffered image with the specs of the chartpanel
                    BufferedImage img = new BufferedImage(rec.width, rec.height,
                            BufferedImage.TYPE_INT_ARGB);
                    Graphics g = img.getGraphics();
                    //paint the chartpanel with the buffered image
                    cp.paint(g);
                    try {
                        //write file as png
                        ImageIO.write(img, "png", file);
                        //pop up menu to confirm replacement was successful
                        JOptionPane.showMessageDialog(null, 
                                "Saved and replaced existing file.", "", 
                                JOptionPane.INFORMATION_MESSAGE);
                    } catch (IOException ex) {
                    }
                }
            } else {
                //Create a rectangle with the bound of the chart panel
                Rectangle rec = cp.getBounds();
                //make a new buffered image with the specs of the chartpanel
                BufferedImage img = new BufferedImage(rec.width, rec.height, 
                        BufferedImage.TYPE_INT_ARGB);
                Graphics g = img.getGraphics();
                //paint the chartpanel with the buffered image
                cp.paint(g);
                try {
                    //write file as png                    
                    ImageIO.write(img, "png", file);
                    //pop up menu to confirm replacement was successful
                    JOptionPane.showMessageDialog(null, "Saved.", "", 
                            JOptionPane.INFORMATION_MESSAGE);
                } catch (IOException ex) {
                }
            }
        } else {
        }

    }

}
