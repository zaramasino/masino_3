/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package masino_3;

//imports
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.text.DateFormat;
import java.util.Date;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.util.ShapeUtilities;
import java.text.SimpleDateFormat;

/**
 *
 * @author zara
 * a class that allows us to edit the jfreechart created in the chart class
 */
public final class EditChart {
    JFreeChart chart;
    XYPlot plot;
    XYLineAndShapeRenderer renderer;
    TimeSeriesCollection dataset;
    
    /**
     *constructor that creates an editchart object and assigns its basic information
     * @param original the chart object passed into the editchart method
     */
    public EditChart(Chart original) {
        chart = original.getJFreeChart();
        plot = original.getPlot();
        renderer = original.getRenderer();
        plot.setRenderer(renderer);
        changeSeriesColor(0, Color.RED);
        changeSeriesColor(1, Color.ORANGE);
        changeSeriesColor(2, Color.YELLOW);
        changeSeriesColor(3, Color.GREEN);
        changeSeriesColor(4, Color.BLUE);
        changeSeriesColor(5, Color.PINK);
        changeBackgroundColor(Color.LIGHT_GRAY);
        changeGridColor(Color.WHITE);
    }

    /**
     *method that sets the series given visible
     * @param series int representing the desired series
     */
    public void showSeries(int series) {
        renderer.setSeriesVisible(series, true);
    }

    /**
     *method that set the series given invisible
     * @param series int representing the desired series
     */
    public void hideSeries(int series) {
        renderer.setSeriesVisible(series, false);
    }

    /**
     *method that changes the color of the given series to a given color
     * @param series int representing the desired series
     * @param color the desired Color
     */
    public void changeSeriesColor(int series, Color color) {
        renderer.setSeriesPaint(series, color);
    }

    /**
     *method that changes the background color of the plot
     * @param color the desired Color
     */
    public void changeBackgroundColor(Color color) {
        plot.setBackgroundPaint(color);
    }

    /**
     *method that changes the title of the chart to a given string
     * @param text the desired name
     */
    public void changeTitle(String text) {
        chart.setTitle(text);
    }

    /**
     *getter for the title of the chart
     * @return the title as a string
     */
    public String getTitle() {
        return chart.getTitle().getText();
    }

    /**
     *setter for the lower limit of the range
     * credit: https://stackoverflow.com/questions/7231824/setting-range-for-x-y-axis-jfreechart
     * @param i the desired lower range
     */
    public void setlowerRange(int i) {
        plot.getRangeAxis().setLowerBound(i);
    }

    /**
     *setter for the upper limit of the range
     * credit: https://stackoverflow.com/questions/7231824/setting-range-for-x-y-axis-jfreechart
     * @param i the desired upper range
     */
    public void setupperRange(int i) {
        plot.getRangeAxis().setUpperBound(i);
    }


    /**
     *getter for the lower limit of the range
     * credit: https://www.journaldev.com/18380/java-convert-double-to-string
     * @return the lower range as a string
     */
    public String getlowerRange() {
        double d = plot.getRangeAxis().getLowerBound();
        String str = d + "";
        return str;
    }
    /**
     *getter for the lower limit of the range
     * credit: https://www.journaldev.com/18380/java-convert-double-to-string
     * @return the lower range as a string
     */
    public String getupperRange() {
        double d = plot.getRangeAxis().getUpperBound();
        String str = d + "";
        return str;
    }


    /**
     *sets the lower limit of the domain
     * credit: https://www.codegrepper.com/code-examples/java/how+to+split+integers+from+string+in+java
     * credit: https://www.jfree.org/jfreechart/api/javadoc/org/jfree/chart/axis/DateAxis.html#getMaximumDate--
     * credit: https://www.javatpoint.com/java-string-to-int
     * @param input the desired lower date
     */
    public void setlowerDomain(String input) {
        //create and split an array of the string and parse it into a date
        String[] strArray = input.split("/");
        Date end = new Date(Integer.parseInt(strArray[2])-1900, 
                Integer.parseInt(strArray[1]),Integer.parseInt(strArray[0]));
        DateAxis domain = (DateAxis) plot.getDomainAxis();
        domain.setMinimumDate(end);
       
    }

    /**
     *sets the upper limit of the domain
     * credit: https://www.codegrepper.com/code-examples/java/how+to+split+integers+from+string+in+java
     * credit: https://www.jfree.org/jfreechart/api/javadoc/org/jfree/chart/axis/DateAxis.html#getMaximumDate--
     * credit: https://www.javatpoint.com/java-string-to-int
     * @param input the desired upper date
     */
    public void setupperDomain(String input) {
        //create and split an array of the string and parse it into a date
        String[] strArray = input.split("/");
        Date end = new Date(Integer.parseInt(strArray[2])-1900, 
                Integer.parseInt(strArray[1]),Integer.parseInt(strArray[0]));
        DateAxis domain = (DateAxis) plot.getDomainAxis();
        domain.setMaximumDate(end);
    }
    
    /**
     *gets the upper limit of the domain
     * credit: https://docs.oracle.com/javase/8/docs/api/java/text/SimpleDateFormat.html
     * credit: https://www.geeksforgeeks.org/how-to-convert-date-to-string-in-java/
     * credit: https://docs.oracle.com/javase/6/docs/api/java/util/Date.html#Date(int,%20int,%20int)
     * @return the upper date as a string
     */
    public String getupperDomain(){
        //get the current date and convert to string format 
        DateAxis domain = (DateAxis) plot.getDomainAxis();
        Date upper = domain.getMaximumDate();
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String dateToString = df.format(upper);
        return dateToString;
        
    }
    /**
     *gets the lower limit of the domain
     * credit: https://docs.oracle.com/javase/8/docs/api/java/text/SimpleDateFormat.html
     * credit: https://www.geeksforgeeks.org/how-to-convert-date-to-string-in-java/
     * credit: https://docs.oracle.com/javase/6/docs/api/java/util/Date.html#Date(int,%20int,%20int)
     * @return the lower date as a string
     */
     public String getlowerDomain(){
         //get the current date and convert to string format
        DateAxis domain = (DateAxis) plot.getDomainAxis();
        Date upper = domain.getMinimumDate();
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String dateToString = df.format(upper);
        return dateToString;
        
    }

    /**
     *method that changes the name of the x axis to a given string
     * credit: https://www.jfree.org/forum/viewtopic.php?t=18659
     * @param text the desired name
     */
    public void changeXName(String text) {
        plot.getDomainAxis().setLabel(text);
    }

    /**
     *method that changes the name of the y axis to a given string
     * credit: https://www.jfree.org/forum/viewtopic.php?t=18659
     * @param text the desired name
     */
    public void changeYName(String text) {
        plot.getRangeAxis().setLabel(text);
    }

    /**
     *method that changes the color of the grid lines to a given color
     * @param color the desired color
     */
    public void changeGridColor(Color color) {
        plot.setRangeGridlinePaint(color);
        plot.setDomainGridlinePaint(color);
    }

    /**
     *method that sets the series shape to a diamond
     * credit: https://www.tabnine.com/code/java/methods/org.jfree.chart.renderer.xy.XYLineAndShapeRenderer/setSeriesShape 
     * @param series int representing the desired series
     */
    public void diamondIcon(int series) {
        renderer.setSeriesShape(series, ShapeUtilities.createDiamond(5));
    }

    /**
     *method that sets the series shape to a square
     * credit: https://www.jfree.org/forum/viewtopic.php?t=2411
     * @param series int representing the desired series
     */
    public void squareIcon(int series) {
        renderer.setSeriesShape(series, new Rectangle2D.Double(-3.0, -3.0, 6.0, 6.0));
    }

    /**
     *method that sets the series shape to a circle
     * credit: https://www.jfree.org/forum/viewtopic.php?t=2411
     * @param series int representing the desired series
     */
    public void circleIcon(int series) {
        renderer.setSeriesShape(series, new Ellipse2D.Double(-3.0, -3.0, 6.0, 6.0));
    }

    /**
     *method that sets the series shape to a cross
     * credit: https://stackoverflow.com/questions/6665354/changing-the-shapes-of-points-in-scatter-plot
     * @param series int representing the desired series
     */
    public void crossIcon(int series) {
        renderer.setSeriesShape(series, ShapeUtilities.createDiagonalCross(3, 1));
    }

    /**
     *method that changes the line type to solid
     * credit: https://stackoverflow.com/questions/844988/dashed-line-in-jfreechart
     * @param series int representing the desired series
     */
    public void solidLine(int series) {
        renderer.setSeriesStroke(
                series,
                new BasicStroke(2.0f)
        );
    }


    /**
     *method that sets line type to dashed
     * credit: https://stackoverflow.com/questions/844988/dashed-line-in-jfreechart
     * @param series int representing the desired series
     */
    public void dashedLine(int series) {
        renderer.setSeriesStroke(
                series,
                new BasicStroke(
                        2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
                        1.0f, new float[]{2.0f, 6.0f}, 0.0f
                )
        );
    }
    
    /*
    reverts all the specifications to the default
    */
    public void revert(){   
        changeBackgroundColor(Color.LIGHT_GRAY);
        changeGridColor(Color.WHITE);  
        squareIcon(0);
        squareIcon(1);
        squareIcon(2);
        squareIcon(3);
        squareIcon(4);
        squareIcon(5);
        showSeries(0);
        showSeries(1);
        showSeries(2);
        showSeries(3);
        showSeries(4);
        showSeries(5);
        changeSeriesColor(0, Color.RED);
        changeSeriesColor(1, Color.ORANGE);
        changeSeriesColor(2, Color.YELLOW);
        changeSeriesColor(3, Color.GREEN);
        changeSeriesColor(4, Color.BLUE);
        changeSeriesColor(5, Color.PINK);
        solidLine(0);
        solidLine(1);
        solidLine(2);
        solidLine(3);
        solidLine(4);
        solidLine(5);
        changeGridColor(Color.GRAY);
        changeTitle("Temperature and Precipitation in Canada");
        changeXName("Date");
        changeYName("Number");
    }
 
    /**
     *method that returns the JFreeChart 
     * @return JFreeChart chart
     */
    public JFreeChart getJFreeChart() {
        return chart;
    }

}
